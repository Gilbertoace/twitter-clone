import React from 'react';
import { View } from 'react-native';
import { UserType } from '../../../types';
import ProfilePicture from '../../ProfilePicture';

export type LeftContainerProps = {
  user: UserType,
}

const LeftContainer = ({ user }: LeftContainerProps) => (
  <View>
     <ProfilePicture image={user.image || "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQIbfDzNPtnPQF6u02N9c4z9QvRUPlIFGu91A&usqp=CAU"} size={75} />
  </View>
)

export default LeftContainer;