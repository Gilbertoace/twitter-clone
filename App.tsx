import { StatusBar } from "expo-status-bar";
import React, { useEffect } from "react";
import { SafeAreaProvider } from "react-native-safe-area-context";
import Amplify, { Auth, API, graphqlOperation } from "aws-amplify";
import { withAuthenticator } from "aws-amplify-react-native";

import useCachedResources from "./hooks/useCachedResources";
import useColorScheme from "./hooks/useColorScheme";
import Navigation from "./navigation";
import config from "./src/aws-exports";
import { getUser } from './graphql/queries';
import { createUser } from './graphql/mutations';
import { CreateUserInput } from "./API";

Amplify.configure(config);

function App() {
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();


  const constImageForAllUsers = () => {
    return "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQIbfDzNPtnPQF6u02N9c4z9QvRUPlIFGu91A&usqp=CAU";
  } 

  const saveUserToDb = async (user: CreateUserInput) => {
    // console.log(user);
    await API.graphql(graphqlOperation(createUser, { input: user}))
  }

  // sync cognito user with the database (appsync)
  useEffect(() => {
    (async () => {
      // Get current authenticated user
      const userInfo = await Auth.currentAuthenticatedUser({bypassCache: true});
      
      // check if user already exists in database
      if(userInfo) {
        const userData = await API.graphql(graphqlOperation(getUser, {id: userInfo.attributes.sub}));

        // if user does not exist, create user in the database
        if(!userData.data.getUser){
          const user = {
            id: userInfo.attributes.sub,
            username: userInfo.username,
            name: userInfo.username,
            email: userInfo.attributes.email,
            image: constImageForAllUsers(),
          }
          await saveUserToDb(user);
        }else {
          console.log('User already exists');
        }
      }
    })();
  }, []);

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <SafeAreaProvider>
        <Navigation colorScheme={colorScheme} />
        <StatusBar />
      </SafeAreaProvider>
    );
  }
}

export default withAuthenticator(App);
