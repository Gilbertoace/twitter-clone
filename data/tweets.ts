const tweets = [
    {
      id: 't1',
      user: {
        id: 'u1',
        username: 'SavinVadim_',
        name: 'Vadim Savin',
        image: 'https://miro.medium.com/max/1400/0*0fClPmIScV5pTLoE.jpg'
      },
      createdAt: '2020-08-27T12:00:00.000Z',
      content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
      image: 'https://i.insider.com/5d03aa8e6fc9201bc7002b43?width=1136&format=jpeg',
      numberOfComments: 123,
      numberOfRetweets: 11,
      numberOfLikes: 10,
    }, {
      id: 't2',
      user: {
        id: 'u1',
        username: 'SavinVadim_',
        name: 'Vadim Savin',
        image: 'https://d2qp0siotla746.cloudfront.net/img/use-cases/profile-picture/template_3.jpg'
      },
      createdAt: '2020-08-27T12:00:00.000Z',
      content: 'Hey Hey Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
      numberOfComments: 4,
      numberOfRetweets: 11,
      numberOfLikes: 99,
    }, {
      id: 't3',
      user: {
        id: 'u1',
        username: 'SavinVadim_',
        name: 'Vadim Savin',
        image: 'https://www.elitesingles.co.uk/wp-content/uploads/sites/59/2019/11/2b_en_articleslide_sm2-350x264.jpg'
      },
      createdAt: '2020-08-27T12:00:00.000Z',
      content: 'Hello World',
      numberOfComments: 4,
      numberOfRetweets: 11,
      numberOfLikes: 99,
    }, {
      id: 't4',
      user: {
        id: 'u1',
        username: 'SavinVadim_',
        name: 'Vadim Savin',
        image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRBwgu1A5zgPSvfE83nurkuzNEoXs9DMNr8Ww&usqp=CAU'
      },
      createdAt: '2020-08-27T12:00:00.000Z',
      content: 'Hey Hey Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.',
      numberOfComments: 4,
      numberOfRetweets: 11,
      numberOfLikes: 99,
    }
  ];
  
  export default tweets;